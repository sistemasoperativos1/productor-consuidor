/********************************************************************************
** Form generated from reading UI file 'configu.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONFIGU_H
#define UI_CONFIGU_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>

QT_BEGIN_NAMESPACE

class Ui_Configu
{
public:
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLabel *label;
    QLabel *label_2;

    void setupUi(QDialog *Configu)
    {
        if (Configu->objectName().isEmpty())
            Configu->setObjectName(QStringLiteral("Configu"));
        Configu->resize(458, 263);
        lineEdit = new QLineEdit(Configu);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(280, 170, 113, 27));
        lineEdit_2 = new QLineEdit(Configu);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(280, 80, 113, 27));
        label = new QLabel(Configu);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(70, 90, 121, 21));
        label_2 = new QLabel(Configu);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(70, 180, 141, 21));

        retranslateUi(Configu);

        QMetaObject::connectSlotsByName(Configu);
    } // setupUi

    void retranslateUi(QDialog *Configu)
    {
        Configu->setWindowTitle(QApplication::translate("Configu", "Configuraci\303\263n", nullptr));
        label->setText(QApplication::translate("Configu", "Nro. Productores", nullptr));
        label_2->setText(QApplication::translate("Configu", "Nro. Consumidores", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Configu: public Ui_Configu {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONFIGU_H
