/********************************************************************************
** Form generated from reading UI file 'dleeme.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLEEME_H
#define UI_DLEEME_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Dleeme
{
public:
    QLabel *label;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;

    void setupUi(QDialog *Dleeme)
    {
        if (Dleeme->objectName().isEmpty())
            Dleeme->setObjectName(QStringLiteral("Dleeme"));
        Dleeme->resize(621, 233);
        label = new QLabel(Dleeme);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 10, 231, 41));
        widget = new QWidget(Dleeme);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(10, 90, 598, 121));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(widget);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout->addWidget(label_2);

        label_3 = new QLabel(widget);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout->addWidget(label_3);

        label_4 = new QLabel(widget);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout->addWidget(label_4);

        label_5 = new QLabel(widget);
        label_5->setObjectName(QStringLiteral("label_5"));

        verticalLayout->addWidget(label_5);

        label_6 = new QLabel(widget);
        label_6->setObjectName(QStringLiteral("label_6"));

        verticalLayout->addWidget(label_6);


        retranslateUi(Dleeme);

        QMetaObject::connectSlotsByName(Dleeme);
    } // setupUi

    void retranslateUi(QDialog *Dleeme)
    {
        Dleeme->setWindowTitle(QApplication::translate("Dleeme", "L\303\251eme", nullptr));
        label->setText(QApplication::translate("Dleeme", "Problema Productor-Consumidor", nullptr));
        label_2->setText(QApplication::translate("Dleeme", "Los productores se encuentran en el lado derecho y los consumidores en el lado izquierdo.", nullptr));
        label_3->setText(QApplication::translate("Dleeme", "Existe un productor y un consumidor por defecto", nullptr));
        label_4->setText(QApplication::translate("Dleeme", "El bot\303\263n m\303\241s grande de cada lado sirve para agregar m\303\241s produtores o consumidores", nullptr));
        label_5->setText(QApplication::translate("Dleeme", "El bot\303\263n Iniciar o Continuar ejecuta o comienza un hilo respectivamente ", nullptr));
        label_6->setText(QApplication::translate("Dleeme", "El bot\303\263n pausa detiene el hilo y el bot\303\263n Detener lo elimina", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Dleeme: public Ui_Dleeme {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DLEEME_H
