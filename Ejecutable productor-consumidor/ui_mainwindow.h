/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionL_eme;
    QAction *actionConfiguraci_n;
    QAction *actionAcerca_de;
    QWidget *centralWidget;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *Iniciar;
    QPushButton *Pausar;
    QLabel *label_5;
    QTextEdit *textEdit;
    QPushButton *pushButton_4;
    QPushButton *pushButton_6;
    QLabel *label_6;
    QWidget *widget;
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *AgreProd;
    QFrame *ProdFrame;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;
    QProgressBar *bufferProgressBar;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *AgreCons;
    QFrame *ProdFrame_2;
    QVBoxLayout *verticalLayout_4;
    QSpacerItem *verticalSpacer_2;
    QWidget *widget1;
    QHBoxLayout *horizontalLayout_8;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label;
    QLineEdit *lineEdit;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_2;
    QLineEdit *Pex;
    QWidget *widget2;
    QHBoxLayout *horizontalLayout_10;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_3;
    QLineEdit *lineEdit_3;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_4;
    QLineEdit *Cex;
    QMenuBar *menuBar;
    QMenu *menuPrograma;
    QToolBar *mainToolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(962, 492);
        actionL_eme = new QAction(MainWindow);
        actionL_eme->setObjectName(QStringLiteral("actionL_eme"));
        actionConfiguraci_n = new QAction(MainWindow);
        actionConfiguraci_n->setObjectName(QStringLiteral("actionConfiguraci_n"));
        actionAcerca_de = new QAction(MainWindow);
        actionAcerca_de->setObjectName(QStringLiteral("actionAcerca_de"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 40, 935, 72));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        Iniciar = new QPushButton(layoutWidget);
        Iniciar->setObjectName(QStringLiteral("Iniciar"));

        horizontalLayout->addWidget(Iniciar);

        Pausar = new QPushButton(layoutWidget);
        Pausar->setObjectName(QStringLiteral("Pausar"));

        horizontalLayout->addWidget(Pausar);

        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout->addWidget(label_5);

        textEdit = new QTextEdit(layoutWidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));

        horizontalLayout->addWidget(textEdit);

        pushButton_4 = new QPushButton(layoutWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));

        horizontalLayout->addWidget(pushButton_4);

        pushButton_6 = new QPushButton(layoutWidget);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));

        horizontalLayout->addWidget(pushButton_6);

        label_6 = new QLabel(layoutWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout->addWidget(label_6);

        widget = new QWidget(centralWidget);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(10, 130, 931, 291));
        horizontalLayout_5 = new QHBoxLayout(widget);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        AgreProd = new QPushButton(widget);
        AgreProd->setObjectName(QStringLiteral("AgreProd"));

        horizontalLayout_2->addWidget(AgreProd);


        verticalLayout_2->addLayout(horizontalLayout_2);

        ProdFrame = new QFrame(widget);
        ProdFrame->setObjectName(QStringLiteral("ProdFrame"));
        ProdFrame->setFrameShape(QFrame::StyledPanel);
        ProdFrame->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(ProdFrame);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalSpacer = new QSpacerItem(288, 198, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        verticalLayout_2->addWidget(ProdFrame);


        horizontalLayout_5->addLayout(verticalLayout_2);

        bufferProgressBar = new QProgressBar(widget);
        bufferProgressBar->setObjectName(QStringLiteral("bufferProgressBar"));
        bufferProgressBar->setFocusPolicy(Qt::NoFocus);
        bufferProgressBar->setValue(24);

        horizontalLayout_5->addWidget(bufferProgressBar);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        AgreCons = new QPushButton(widget);
        AgreCons->setObjectName(QStringLiteral("AgreCons"));

        horizontalLayout_3->addWidget(AgreCons);


        verticalLayout_3->addLayout(horizontalLayout_3);

        ProdFrame_2 = new QFrame(widget);
        ProdFrame_2->setObjectName(QStringLiteral("ProdFrame_2"));
        ProdFrame_2->setFrameShape(QFrame::StyledPanel);
        ProdFrame_2->setFrameShadow(QFrame::Raised);
        verticalLayout_4 = new QVBoxLayout(ProdFrame_2);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalSpacer_2 = new QSpacerItem(288, 198, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_2);


        verticalLayout_3->addWidget(ProdFrame_2);


        horizontalLayout_5->addLayout(verticalLayout_3);

        widget1 = new QWidget(centralWidget);
        widget1->setObjectName(QStringLiteral("widget1"));
        widget1->setGeometry(QRect(10, 10, 294, 31));
        horizontalLayout_8 = new QHBoxLayout(widget1);
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label = new QLabel(widget1);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_4->addWidget(label);

        lineEdit = new QLineEdit(widget1);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        horizontalLayout_4->addWidget(lineEdit);


        horizontalLayout_8->addLayout(horizontalLayout_4);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        label_2 = new QLabel(widget1);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_6->addWidget(label_2);

        Pex = new QLineEdit(widget1);
        Pex->setObjectName(QStringLiteral("Pex"));

        horizontalLayout_6->addWidget(Pex);


        horizontalLayout_8->addLayout(horizontalLayout_6);

        widget2 = new QWidget(centralWidget);
        widget2->setObjectName(QStringLiteral("widget2"));
        widget2->setGeometry(QRect(650, 10, 293, 31));
        horizontalLayout_10 = new QHBoxLayout(widget2);
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        label_3 = new QLabel(widget2);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_7->addWidget(label_3);

        lineEdit_3 = new QLineEdit(widget2);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));

        horizontalLayout_7->addWidget(lineEdit_3);


        horizontalLayout_10->addLayout(horizontalLayout_7);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        label_4 = new QLabel(widget2);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_9->addWidget(label_4);

        Cex = new QLineEdit(widget2);
        Cex->setObjectName(QStringLiteral("Cex"));

        horizontalLayout_9->addWidget(Cex);


        horizontalLayout_10->addLayout(horizontalLayout_9);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 962, 24));
        menuPrograma = new QMenu(menuBar);
        menuPrograma->setObjectName(QStringLiteral("menuPrograma"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);

        menuBar->addAction(menuPrograma->menuAction());
        menuPrograma->addAction(actionL_eme);
        menuPrograma->addAction(actionAcerca_de);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionL_eme);
        mainToolBar->addAction(actionAcerca_de);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Productor Consumidor", nullptr));
        actionL_eme->setText(QApplication::translate("MainWindow", "L\303\251eme", nullptr));
        actionConfiguraci_n->setText(QApplication::translate("MainWindow", "Configuraci\303\263n", nullptr));
        actionAcerca_de->setText(QApplication::translate("MainWindow", "Acerca de", nullptr));
        Iniciar->setText(QApplication::translate("MainWindow", "Play", nullptr));
        Pausar->setText(QApplication::translate("MainWindow", "Pause", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "Productor", nullptr));
        pushButton_4->setText(QApplication::translate("MainWindow", "Play", nullptr));
        pushButton_6->setText(QApplication::translate("MainWindow", "Pause", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "Consumidor", nullptr));
        AgreProd->setText(QApplication::translate("MainWindow", "Agr. Productor", nullptr));
        AgreCons->setText(QApplication::translate("MainWindow", "Agr. Consumidor", nullptr));
        label->setText(QApplication::translate("MainWindow", "#", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "E", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "#", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "E", nullptr));
        menuPrograma->setTitle(QApplication::translate("MainWindow", "Programa", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
