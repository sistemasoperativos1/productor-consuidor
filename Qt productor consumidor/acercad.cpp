#include "acercad.h"
#include "ui_acercad.h"
#include "qpixmap.h"
Acercad::Acercad(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Acercad)
{
    ui->setupUi(this);
    QPixmap pix("logo.png");
    ui->label_8->setPixmap(pix);
}

Acercad::~Acercad()
{
    delete ui;
}
