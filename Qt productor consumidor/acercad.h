#ifndef ACERCAD_H
#define ACERCAD_H

#include <QDialog>

namespace Ui {
class Acercad;
}

class Acercad : public QDialog
{
    Q_OBJECT

public:
    explicit Acercad(QWidget *parent = nullptr);
    ~Acercad();

private:
    Ui::Acercad *ui;
};

#endif // ACERCAD_H
