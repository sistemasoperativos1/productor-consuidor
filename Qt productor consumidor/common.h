#ifndef COMMON_H
#define COMMON_H


#include <QSemaphore>
#include "myconstants.h"

extern char buffer[BufferSize];

extern QSemaphore freeBytes;
extern QSemaphore usedBytes;
const int TProd=0;
extern int TCons;


#endif // COMMON_H
