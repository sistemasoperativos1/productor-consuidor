#ifndef CONFIGU_H
#define CONFIGU_H

#include <QDialog>

namespace Ui {
class Configu;
}

class Configu : public QDialog
{
    Q_OBJECT

public:
    explicit Configu(QWidget *parent = nullptr);
    ~Configu();

private:
    Ui::Configu *ui;
};

#endif // CONFIGU_H
