#include "consumidor.h"
#include <QDebug>
Consumidor::Consumidor(QObject *parent) : QObject(parent)
{
    nConsumidor = new Consumer;

    connect(nConsumidor, SIGNAL(bufferFillCountChanged(int)),
              this, SLOT(onBufferValueChanged(int)));

    // connect signal/slot for consumer/producer progress bar

    connect(nConsumidor, SIGNAL(consumerCountChanged(int)),
              this, SLOT(onProducerValueChanged(int)));

}

void Consumidor::InitCons(){
    nConsumidor->start();
    printf("inicio");
}
void Consumidor::PauCons(){
    nConsumidor->stop=true;
}

void Consumidor::onBufferValueChanged(int Cant){
    emit bufferCont(Cant);
}
void Consumidor::onProducerValueChanged(int Cant){
    emit CargaCont(Cant);
}
