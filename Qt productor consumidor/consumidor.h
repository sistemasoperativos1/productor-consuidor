#ifndef CONSUMIDOR_H
#define CONSUMIDOR_H

#include <QObject>
#include "consumer.h"
#include<QtDebug>

class Consumidor : public QObject
{
    Q_OBJECT
public:
    explicit Consumidor(QObject *parent = nullptr);
    void CreaProd();

signals:
    void bufferCont(int cont);
    void CargaCont(int cont);
public slots:
    void InitCons();
    void PauCons();
    void onBufferValueChanged(int);
    void onProducerValueChanged(int);
private:
    Consumer *nConsumidor;
};

#endif // CONSUMIDOR_H
