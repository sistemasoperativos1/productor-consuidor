#ifndef DLEEME_H
#define DLEEME_H

#include <QDialog>

namespace Ui {
class Dleeme;
}

class Dleeme : public QDialog
{
    Q_OBJECT

public:
    explicit Dleeme(QWidget *parent = nullptr);
    ~Dleeme();

private:
    Ui::Dleeme *ui;
};

#endif // DLEEME_H
