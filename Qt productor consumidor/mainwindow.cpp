#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "myconstants.h"
#include "productor.h"
#include "consumidor.h"
#include "QStringList"
#include "QString"
// BufferSize: maximum bytes that can be stored
char buffer[BufferSize];

QSemaphore freeBytes(BufferSize);
QSemaphore usedBytes;
int CantProd=0;
int CantCons =0;
int CantEx=0;
QStringList lista;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    // progress bar range setup
    ui->bufferProgressBar->setRange(0, BufferSize);

    // make two threads
    mProducer = new Producer(this);
    mConsumer = new Consumer(this);

    // connect signal/slot for the buffer progress bar
    connect(mConsumer, SIGNAL(bufferFillCountChanged(int)),
              this, SLOT(onBufferValueChanged(int)));
    connect(mProducer, SIGNAL(bufferFillCountChanged(int)),
              this, SLOT(onBufferValueChanged(int)));

    // connect signal/slot for consumer/producer progress bar
    connect(mConsumer, SIGNAL(consumerCountChanged(int)),
              this, SLOT(onConsumerValueChanged(int)));
    connect(mProducer, SIGNAL(producerCountChanged(int)),
              this, SLOT(onProducerValueChanged(int)));

    QObject::connect(
                ui->AgreProd, &QPushButton::clicked,
                this, &MainWindow::onAddConsum);
    QObject::connect(
                ui->AgreCons, &QPushButton::clicked,
                this, &MainWindow::onAddProd);

}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::onBufferValueChanged(int bCount)
{
    ui->bufferProgressBar->setValue(bCount);
    if (bCount >= BufferSize){
        CantEx = CantEx +1;
        ui->Pex->setText(QString::number(CantEx));
    }
}

void MainWindow::onProducerValueChanged(int pCount)
{
    CantProd= CantProd +1;
    ui->lineEdit->setText(QString::number( CantProd));
    lista<<QString( "ACGT"[(int)qrand() % 4]);
    QString listS = lista.join(",");
    ui->textEdit->setText(listS);
}

void MainWindow::onConsumerValueChanged(int cCount)
{
    CantCons = CantCons +1;
    ui->lineEdit_3->setText(QString::number( CantCons ));
    lista.removeLast();
    QString listS = lista.join(",");
    ui->textEdit->setText(listS);
}


void MainWindow::on_actionL_eme_triggered()
{
    Dleeme *VentLeeme = new Dleeme(this);
    VentLeeme->setModal(true);
    VentLeeme->show();
}

void MainWindow::on_actionAcerca_de_triggered()
{
    Acercad *VentAcerca = new Acercad(this);
    VentAcerca->setModal(true);
    VentAcerca->show();
}

void MainWindow::on_actionConfiguraci_n_triggered()
{
    Configu *VentConfig = new Configu(this);
    VentConfig->setModal(true);
    VentConfig->show();
}

void MainWindow::on_Iniciar_clicked()
{
    mProducer->start();
}

void MainWindow::on_Parar_clicked()
{

}

void MainWindow::on_pushButton_2_clicked()
{

}

void MainWindow::on_pushButton_4_clicked()
{
    mConsumer->start();

}

void MainWindow::on_pushButton_5_clicked()
{

}

void MainWindow::onAddConsum(){
    QVBoxLayout* layout = qobject_cast<QVBoxLayout*>(ui->ProdFrame->layout());
    QHBoxLayout* newLayout = new QHBoxLayout(ui->ProdFrame);

    QPushButton* Cont = new QPushButton("Continuar", ui->ProdFrame);
    newLayout->addWidget(Cont);
    QPushButton* Pau = new QPushButton("Pausar", ui->ProdFrame);
    newLayout->addWidget(Pau);
    QPushButton* Det = new QPushButton("Detener", ui->ProdFrame);
    newLayout->addWidget(Det);
    productor *NuevP = new productor;
    layout->insertLayout(0,newLayout);
    mButtonToLayoutMap.insert(Det,newLayout);
    QObject::connect(
                Det, &QPushButton::clicked,
                this, &MainWindow::onRemoveConsum);
    connect(NuevP, SIGNAL(bufferCont(int)),
              this, SLOT(onBufferValueChanged(int)));
    connect(NuevP, SIGNAL(CargaCont(int)),
              this, SLOT(onProducerValueChanged(int)));
    connect(
                Cont, SIGNAL( clicked()),
                NuevP,SLOT(InitProd()));
    connect(
                Pau, SIGNAL(clicked()),
                NuevP,SLOT(PauProd()));

}

void MainWindow::onRemoveConsum(){
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    QHBoxLayout* layout = mButtonToLayoutMap.take(button);

    while (layout->count() !=0) {
        QLayoutItem* item = layout->takeAt(0);
        delete  item->widget();
        delete item;

    }

    delete layout;
}

void MainWindow::correrHilo(Producer *HiloP){
    HiloP->start();
}
void MainWindow::on_Pausar_clicked()
{
    mProducer->stop=true;
}

void MainWindow::onAddProd(){
    QVBoxLayout* layout = qobject_cast<QVBoxLayout*>(ui->ProdFrame_2->layout());
    QHBoxLayout* newLayout = new QHBoxLayout(ui->ProdFrame_2);

    QPushButton* Cont = new QPushButton("Continuar", ui->ProdFrame_2);
    newLayout->addWidget(Cont);
    QPushButton* Pau = new QPushButton("Pausar", ui->ProdFrame_2);
    newLayout->addWidget(Pau);
    QPushButton* Det = new QPushButton("Detener", ui->ProdFrame_2);
    newLayout->addWidget(Det);
    Consumidor *NuevC = new Consumidor;
    layout->insertLayout(0,newLayout);
    mButtonToLayoutMap2.insert(Det,newLayout);
    QObject::connect(
                Det, &QPushButton::clicked,
                this, &MainWindow::onRemoveConsum);
    connect(NuevC, SIGNAL(bufferCont(int)),
              this, SLOT(onBufferValueChanged(int)));
    connect(NuevC, SIGNAL(CargaCont(int)),
              this, SLOT(onConsumerValueChanged(int)));



    connect(
                Cont, SIGNAL( clicked()),
                NuevC,SLOT(InitCons()));
    connect(
                Pau, SIGNAL(clicked()),
                NuevC,SLOT(PauCons()));

}

void MainWindow::onRemoveProd(){
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    QHBoxLayout* layout = mButtonToLayoutMap2.take(button);

    while (layout->count() !=0) {
        QLayoutItem* item = layout->takeAt(0);
        delete  item->widget();
        delete item;

    }

    delete layout;
}

void MainWindow::on_pushButton_6_clicked()
{
    mConsumer->stop=true;
}
