#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <dleeme.h>
#include <acercad.h>
#include <configu.h>
#include <QDialog>
#include "consumer.h"
#include "ui_mainwindow.h"
#include "producer.h"

#include <QSemaphore>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void onAddConsum();
    void onRemoveConsum();
    void onAddProd();
    void onRemoveProd();
    void correrHilo(Producer *HiloP);

private slots:
    void on_actionL_eme_triggered();

    void on_actionAcerca_de_triggered();

    void on_actionConfiguraci_n_triggered();

    void on_Iniciar_clicked();

    void on_Parar_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void onBufferValueChanged(int);
    void onProducerValueChanged(int);
    void onConsumerValueChanged(int);

    void on_Pausar_clicked();

    void on_pushButton_6_clicked();

private:
    Ui::MainWindow *ui;
    Producer *mProducer;
    Consumer *mConsumer;
    QHash<QPushButton*, QHBoxLayout*> mButtonToLayoutMap;
    QHash<QPushButton*, QHBoxLayout*> mButtonToLayoutMap2;

};

#endif // MAINWINDOW_H
