#include "producer.h"
#include "common.h"
#include <QDebug>
Producer::Producer(QObject *parent) :
    QThread(parent)
{
}

void Producer::run()
{
    //qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
    this->stop = false;
    while (true)  {
        freeBytes.acquire();
        //buffer[i % BufferSize] = "ACGT"[(int)qrand() % 4];
        if (this->stop) break;
        usedBytes.release();
        int total = usedBytes.available();
        emit bufferFillCountChanged(total);
        emit producerCountChanged(1);
        msleep(250);
    }
}

