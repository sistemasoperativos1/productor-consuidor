#include "productor.h"
#include <QDebug>

productor::productor(QObject *parent) : QObject(parent)
{
    nProductor = new Producer;


    connect(nProductor, SIGNAL(bufferFillCountChanged(int)),
              this, SLOT(onBufferValueChanged(int)));

    // connect signal/slot for consumer/producer progress bar

    connect(nProductor, SIGNAL(producerCountChanged(int)),
              this, SLOT(onProducerValueChanged(int)));
}
void productor::InitProd(){
    nProductor->start();
    printf("inicio");
}
void productor::PauProd(){
    nProductor->stop=true;
}

void productor::onBufferValueChanged(int Cant){
    emit bufferCont(Cant);
}
void productor::onProducerValueChanged(int Cant){
    emit CargaCont(Cant);
}
