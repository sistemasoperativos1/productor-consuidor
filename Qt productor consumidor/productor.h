#ifndef PRODUCTOR_H
#define PRODUCTOR_H

#include <QObject>
#include "producer.h"
#include <QDebug>
class productor : public QObject
{
    Q_OBJECT
public:
    explicit productor(QObject *parent = nullptr);
    void CreaProd();

signals:
    void bufferCont(int cont);
    void CargaCont(int cont);
public slots:
    void InitProd();
    void PauProd();
    void onBufferValueChanged(int);
    void onProducerValueChanged(int);
private:
    Producer *nProductor;
};

#endif // PRODUCTOR_H
